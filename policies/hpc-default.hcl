# List available secrets engines
path "sys/mounts" {  
  capabilities = [ "read" ]
}


###################### Userpass auth

# To change own password of userpass if exists
path "auth/userpass/users/{{identity.entity.aliases.auth_userpass_a3cecf11.name}}" {  
  capabilities = [ "update" ]  
  allowed_parameters = {    
    "password" = []  
  }
}


###################### Personal secrets

# For listing all personal secret paths
# For sending write-only secrets to others 
path "personal/*" {  
  capabilities = [ "create", "list" ]
}

# For personal secrets
path "personal/{{identity.entity.aliases.auth_oidc_a51e630e.name}}/*" {  
  capabilities = [ "create", "update", "read", "delete", "list" ]
}

# For Web UI usage
path "personal/metadata" {  
  capabilities = ["list"]
}


###################### General secrets

# For listing all secret paths
path "secret/*" {  
  capabilities = [ "list" ]
}

# For hpc-default secrets
path "secret/hpc-default/*" {  
  capabilities = [ "create", "update", "read", "delete", "list" ]
}

# For Web UI usage
path "secret/metadata" {  
  capabilities = ["list"]
}


###################### SSH

# List available SSH roles
path "ssh-client-signer/roles/*" {
 capabilities = [ "list" ]
}

# Allow access to SSH role
path "ssh-client-signer/sign/{{identity.entity.aliases.auth_userpass_a3cecf11.name}}" {
 capabilities = [ "create" , "update" ]
}
