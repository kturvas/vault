# /etc/vault.d/vault.hcl 
# Full configuration options can be found at https://www.vaultproject.io/docs/configuration

ui = true

# https://www.vaultproject.io/docs/configuration#disable_mlock
# mlock prevents memory from being swapped to disk
# By default mlock is enabled
#mlock = true
#disable_mlock = false

storage "raft" {
  path = "/opt/vault/data"
  node_id = "node1"
}

# HTTP listener, for localhost (and internal network)
# Encrypt traffic with CA signed certs...
# ...or disable tls with tls_disable
listener "tcp" {
  address = "localhost:8300"
  #tls_disable = "true"
  tls_cert_file = "/home/centos/pki/issued/localhost.crt" 
  tls_key_file  = "/home/centos/localhost.key"
  tls_client_ca_file = "/home/centos/easy-rsa/pki/ca.crt"
}

# HTTPS listener
listener "tcp" {
  address       = "0.0.0.0:8200"
  tls_cert_file = "/etc/letsencrypt/live/vault.jamfox.dev/fullchain.pem"
  tls_key_file  = "/etc/letsencrypt/live/vault.jamfox.dev/privkey.pem"
}

api_addr = "https://192.168.42.136:8200"
cluster_addr = "https://192.168.42.136:8201"
