# HPC Vault

Vault is an identity-based secrets and encryption management system. A secret is anything that you want to tightly control access to, such as API encryption keys, passwords, or certificates. Vault provides encryption services that are gated by authentication and authorization methods. Using Vault’s UI, CLI, or HTTP API, access to secrets and other sensitive data can be securely stored and managed, tightly controlled (restricted), and auditable.

[Hashicorp Vault documentation](https://www.vaultproject.io/docs/what-is-vault)

HPC demo Vault is accessible via https://vault.jamfox.dev:8200 

To communicate with Vault you can use:
- Webapp GUI by going to https://vault.jamfox.dev:8200 
- Via [API](https://www.vaultproject.io/api-docs).
- Via CLI after installing Vault client on your machine. 

# Self-service demo walkthrough

## Azure AD auth 

The following will only work if you have the `HPC Centre` group associated with your TalTech Uni-ID account. If a user is part of the `HPC Centre` group then they will automatically be assigned the `hpc-default` policy needed for doing any of the following steps. 

Via web browser:
- Go to https://vault.jamfox.dev:8200 on your browser, click on `oidc` and then click `Sign in with OIDC Provider`.

Via CLI: 
- Use the command `vault login -method=oidc` then a browser pop-up should appear.

## Userpass auth

Azure auth is good for checking if user is part of required groups and linking those groups with Vault policies, but userpass auth makes it easier to automate Vault CLI usage.

After authenticating with Azure AD ask for administrator to create an userpass alias for you.

Then set your password for userpass via CLI:
- `vault write auth/userpass/users/<YOUR-USER-PROVIDED-BY-ADMIN-HERE> password="<YOUR-NEW-PASS-HERE>"`

Then log in with it via:
- web UI  
- CLI: 
```
vault login -method=userpass \
    username=<YOUR-USER> \
    password=<YOUR-PASS>
```

## Key/Value secrets

Key/Value secrets are a key part of using Vault (pun intended). The `kv` secrets engine is used to store arbitrary secrets within the configured physical storage for Vault. Writing to a key in the `kv` backend will replace the old value; sub-fields are not merged together.

Current proposal is to use 2 Key/Value engines. One named `personal` for personal secrets and the other named `secret` for general secrets.

According to `hpc-default` policy: 
- Every `HPC Centre` group user can store personal secrets that only they can modify and read at the templated path `personal/{{identity.entity.aliases.<AUTH0-ACCESSOR-HERE>.name}}/*` where `{{identity.entity.aliases.<AUTH0-ACCESSOR-HERE>.name}}` is the associated TalTech Uni-ID email.
- Every `HPC Centre` group user can create write-only secrets to any other users path `personal/*` (Existing values can not be modified/overwritten).

General secrets should follow a defined structure that is agreed upon beforehand. The more granular the paths are, the easier it will be to manage these secrets as the amount of secrets grows. In essence paths should be something like the following example (bad barebones example, but hopefully it gets the essence across well enough):

```
secret/
├── hpc-default/
│   └── base/
│        └── testsecret="Sup3rS3cr3t"
│ 
├── hpc-sysadmin/
│   └── clusters/
│        ├── amp/
│        ├── grey/
│        └── green/
│ 
└── hpc-dev/
    └── invenio/
```

The easiest way to store and read secrets is to use the web UI. So navigate to https://vault.jamfox.dev:8200 and under `Secrets` click on `secret` or `personal` and see what you can find, create some secrets, update them, try to send someone a personal secret. 

## SSH access using vault

SSH roles must match userpass name that was created for you for the permissions to link properly. So, after authenticating with Azure AD and after asking administrator to create an userpass alias for you, ask the administrator to create an SSH role for you.

Then ask Vault to sign your public key. `valid_principals` field MUST match `ssh-client-signer/sign/<YOUR-ROLE-HERE>` role's `allowed_users` that admin has set for you:
```
vault write -field=signed_key ssh-client-signer/sign/<YOUR-USERPASS-NAME> \
 public_key=@$HOME/.ssh/<YOUR-KEY>.pub valid_principals=<YOUR-PRINCIPAL-HERE> > ~/.ssh/<YOUR-KEY>-signed.pub
```

(Optional) View enabled extensions, principals, and metadata of the signed key: 
```
ssh-keygen -Lf ~/.ssh/signed-cert.pub
```

SSH into the host machine using the signed key. You must supply both the signed public key from Vault and the corresponding private key as authentication to the SSH call:
```
ssh -i <YOUR-KEY>-signed.pub -i ~/.ssh/<YOUR-KEY> username@host
```

For the purposes of the demo, ask administrator to add your principal to training headnode. Then try connecting to training headnode: 
```
ssh -i <YOUR-KEY>-signed.pub -i ~/.ssh/<YOUR-KEY> centos@193.40.156.61
```
